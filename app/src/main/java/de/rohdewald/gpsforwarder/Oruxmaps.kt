package de.rohdewald.gpsforwarder

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.GET_ACTIVITIES
import android.net.Uri
import android.os.Environment
import java.io.File


fun locateOruxmaps(): String? {
    val appNames = listOf("com.orux.oruxmapsDonate", "com.orux.oruxmaps")
    val foundNames: MutableList<String> = mutableListOf<String>().toMutableList()
    appNames.forEach { appName ->
        appContext.packageManager.apply {
            try {
                val result: Boolean? = getPackageInfo(appName, GET_ACTIVITIES)?.applicationInfo?.enabled
                result?.let {
                    if (result) {
                        foundNames += appName
                    }
                }
            } catch (e: PackageManager.NameNotFoundException) {
            }
        }
    }
    return foundNames.firstOrNull()
}

fun oruxmapsVersion() = locateOruxmaps()?.let { appName ->
    appContext.packageManager.getPackageInfo(appName, GET_ACTIVITIES)?.versionName
}

internal fun broadcastToOrux(action: String) {
    locateOruxmaps()?.let { appName ->
        try {
            val intentName = when (appName) {
                "com.orux.oruxmaps" -> "com.oruxmaps"
                "com.orux.oruxmapsDonate" -> "com.oruxmapsDonate"
                else -> {
                    assert(false) { " Typo" }; "typo"
                }
            }
            val intent = Intent("$intentName.INTENT_$action")
            intent.setClassName(appName, "com.orux.oruxmaps.actividades.TaskIntentReceiver")
            appContext.sendBroadcast(intent)
        } catch (e: ActivityNotFoundException) {
            logError { "$e" }
        }
    }
}

private fun wantOruxmaps(): Boolean {
    locateOruxmaps() ?: return false
    return prefs.getBoolean("pref_key_start_stop_orux", false)
}

fun startOruxmaps() {
    if (wantOruxmaps()) {
        broadcastToOrux("START_RECORD_NEWTRACK")
        logStartStop(R.string.started_oruxmaps)
        loadGpx("${Environment.getExternalStorageDirectory().path}/Download/Horneburg-Eystrup neu.gpx")
    }
}

fun stopOruxmaps() {
    if (wantOruxmaps()) {
        broadcastToOrux("STOP_RECORD")
        logStartStop(R.string.stopped_oruxmaps)
    }
}

fun continueOruxmaps() {
    if (wantOruxmaps()) {
        broadcastToOrux("START_RECORD_CONTINUE")
        logStartStop(R.string.continuing_oruxmaps)
    }
}

fun loadGpx(pathName: String) {
    Intent(Intent.ACTION_VIEW).apply {
        //   setDataAndType(Uri.fromFile(File(pathName)), "text/xml")
        data = Uri.fromFile(File(pathName))

        // Set flag to give temporary permission to external app to use FileProvider
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        currentMain?.setResult(RESULT_OK)
        //      appContext.startActivity(this)
    }
}
