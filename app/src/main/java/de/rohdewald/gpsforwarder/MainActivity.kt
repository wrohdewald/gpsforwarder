package de.rohdewald.gpsforwarder

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Application
import android.app.NotificationManager
import android.content.*
import android.content.Intent.ACTION_BOOT_COMPLETED
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import org.acra.ACRA
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraDialog
import org.acra.annotation.AcraMailSender
import java.lang.System.currentTimeMillis

// TODO: after adding all new location settings, check usage of default values

// set/cleared by onStart / onStop
@SuppressLint("StaticFieldLeak")
var currentMain: MainActivity? = null

// when ending, we do not kill the process but next time we want to restart fresh.
// So after calling finish(), isAlive is reset
var isAlive: Boolean = false

var atBoot: Boolean = false

lateinit var sender: MapMyTracks

lateinit var appContext: Context

private var prefMinTime = 0L
private var prefMinDistance = 0f

val prefs: SharedPreferences by lazy { PreferenceManager.getDefaultSharedPreferences(appContext) }

@AcraCore(buildConfigClass = BuildConfig::class)
@AcraMailSender(mailTo = "wolfgang@rohdewald.de")
@AcraDialog(resText = R.string.send_crash_text, resPositiveButtonText = R.string.send_crash_report, resNegativeButtonText = R.string.send_no_crash_report)
class MyApplication : Application() {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        ACRA.init(this)
        ACRA.DEV_LOGGING = true
    }
}

class AutoStart : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == ACTION_BOOT_COMPLETED) {
            Intent(context, ForegroundService::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                atBoot = true
                context?.startForegroundService(this)
            }
        }
    }
}

class MainActivity : AppCompatActivity(), android.location.LocationListener, SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        sender.preferenceChanged(sharedPreferences)
        loggerPreferenceChanged()
        preferenceChanged()
    }

    private val locationRequested = 1234
    private var prevLocationTime = 0L
    private var prevAddedDays = 0
    private var prefLocationProvider = ""

    val scroller: ScrollListener = ScrollListener()

    private fun preferenceChanged() {
        // TODO: this default is defined in two places
        val oldLocationProvider = prefLocationProvider
        val oldMinDistance = prefMinDistance
        val oldMinTime = prefMinTime
        var changing = false
        prefLocationProvider = prefs.getString("pref_key_location_provider", null) ?: "gps"
        prefMinDistance = (prefs.getString("pref_key_min_distance", null)
                ?: "20").toFloat()
        prefMinTime = (prefs.getString("pref_key_min_time", null) ?: "2").toLong()
        if (oldLocationProvider != prefLocationProvider) {
            // TODO map location provider to label
            // why do I get extra passes here?
            if (oldLocationProvider != "") {
                logGpsFix { appContext.getString(R.string.now_using_location_provider, prefLocationProvider, oldLocationProvider) }
                changing = true
            }
        }
        if (oldMinDistance != prefMinDistance) {
            logGpsFix { appContext.getString(R.string.now_using_min_distance, prefMinDistance) }
            changing = true
        }
        if (oldMinTime != prefMinTime) {
            logGpsFix { appContext.getString(R.string.now_using_min_time, prefMinTime, oldMinTime) }
            changing = true
        }
        if (changing) {
            logStartStop { "preferenceChanged stops location requests" }
            stopLocationRequests()
            if (::sender.isInitialized)
                if (sender.isForwardingEnabled) {
                    logStartStop { "preferenceChanged starts location requests" }
                    startLocationRequests()
                }
        }
    }

    fun startLocationRequests() {
        try {
            locationManager.requestLocationUpdates(prefLocationProvider, prefMinTime * 1000, prefMinDistance, this)
            logGpsFix { "now requesting location updates every ${(prefMinTime * 1000)   .asTimeDeltaString()} / $prefMinDistance m" }
        } catch (e: SecurityException) {
            logError { "$e" }
        }
    }

    fun stopLocationRequests() {
        locationManager.removeUpdates(this)
        logGpsFix { "locationManager.removeUpdates"}
    }

    fun onClickSettings(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun startForwarding() {
        preferenceChanged()
        invalidateOptionsMenu()
        startLocationRequests()
        ForegroundService.startService(this, getString(R.string.gpsforwarder_is_active))
        if (!status.isNetUsable)
            logError { "[${status.instanceId}] ${appContext.getString(R.string.no_internet)}" }
        sender.start()
        if (sender.isResuming) {
            continueOruxmaps()
            logStartStop { appContext.getString(R.string.gpsforwarder_continuing_after_interruption, sender.currentMmtId) }
        } else {
            startOruxmaps()
            logStartStop(R.string.forwarding_enabled)
        }
    }

    private fun stopForwarding() {
        stopService(Intent(application, ForegroundService::class.java))
        invalidateOptionsMenu()
        logStartStop { "stopForwarding going to stopLocationRequests" }
        stopLocationRequests()
        logStartStop(R.string.forwarding_disabled)
        sender.stop()
        stopOruxmaps()
    }

    fun onClickStart(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        tryStartForwarding()
    }

    private fun tryStartForwarding() {
        status.apply {
            when {
                isDataSaving -> askForDataWhitelisting()
                isBatteryOptimized -> askForBatteryWhitelisting()
                !isLocationEnabled -> {
                    if (hasLocationPermission)
                        askForLocation()
                    else
                        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION), locationRequested)
                }
                else -> startForwarding()
            }
        }
    }

    fun onClickStop(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        AlertDialog.Builder(this).apply {
            setMessage(R.string.really_stop)
            setCancelable(false)
            setPositiveButton(android.R.string.ok) { _, _ ->
                stopForwarding()
            }
            setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
            create()
            show()
        }
    }

    fun onClickClearLog(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        logItems.clear()
        logView?.scrollToEnd()
    }

    fun onClickAbout(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    override fun onProviderEnabled(provider: String) {
        logStartStop { getString(R.string.provider_enabled, provider) }
    }

    override fun onProviderDisabled(provider: String) {
        logStartStop { getString(R.string.provider_disabled, provider) }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        // This is deprecated with API 29: will never be called. But the implementation
        // is still needed because otherwise MainActivity would be abstract
    }

    private fun fix24h(location: Location) {
        val hour = 1000L * 3600L
        val thisTime = currentTimeMillis()
        fun deltaHours() = ((thisTime - location.time) / hour).toInt()

        // if Oruxmaps records more than 24 hours, location.time
        // goes back by 24 hours.
        // TODO: take a closer look. What exactly do I have to restart
        // to avoid this? What happens after 2 days?

        val addedDays = (deltaHours() + 1) / 24
        if (addedDays != 0) {
            if (addedDays != prevAddedDays) {
                location.logTimes()
                val msg = resources.getQuantityString(R.plurals.will_add_days_to_gps, addedDays, addedDays)
                logError(msg)
                notify(msg)
                prevAddedDays = addedDays
            }
            location.time += addedDays * 24 * hour
        }
    }

    override fun onLocationChanged(location: Location) {
        if (altitudeAsCounter) {
            locationCount += 1
            location.altitude = locationCount
        }
        // for whatever reason we are sometimes getting the same location several times.
        // Maybe some interaction between the android studio debugger and the app?
        if (location.time == prevLocationTime) {
            logError { "GPS fix with identical time" }
            return
        }
        // network positions from mobile transmitter are too inaccurate. And we cannot know
        // if it was Wifi or mobile transmitter.
        if (location.provider == "network")
            return
        prevLocationTime = location.time
        fix24h(location)
        if (sender.isForwardingEnabled) {
            logGpsFix { getString(R.string.gps_used, location.provider, location.toLogString()) }
            sender.send(location)
        } else {
            logGpsFix { getString(R.string.gps_ignored, location.toLogString()) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        appContext = applicationContext
        if (atBoot) {
            val prevMmtId = prefs.getString("MmtId", "0") ?: "0"
            logError { "MainActivity.onCreate: atBoot is True, using MmtId $prevMmtId from prefs" }
            if (prevMmtId == "0")
                finishAndRemoveTask()
        }
        prefs.registerOnSharedPreferenceChangeListener(this)
        preferenceChanged()
        loggerPreferenceChanged()
        setupLogger(isAlive)
        logError { "MainActivity.onCreate: isAlive is $isAlive" }
        if (!isAlive) {
            if (atBoot)
                logStartStop(R.string.autostart_after_reboot)
            if (!::sender.isInitialized)
                sender = MapMyTracks()
            sender.initialize()
            logError { "MainActivity.onCreate. sender.isResuming: ${sender.isResuming} " }
            Status.register()
            if (sender.isResuming) {
                // time forwarding was not stopped, try to resume
                tryStartForwarding()
            }
            isAlive = true
            atBoot = false
        }
    }

    override fun onStart() {
        super.onStart()
        currentMain = this
        if (scroller.isAtEnd)
            logView?.scrollToEnd()
    }

    override fun onStop() {
        currentMain = null
        super.onStop()
    }

    override fun finish() {
        isAlive = false
        Status.unregister()
        super.finish()
    }

    override fun onDestroy() {
        prefs.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroy()
    }

    private fun askForLocation() {
        AlertDialog.Builder(this).apply {
            setMessage(R.string.enable_location_service)
            setCancelable(false)
            setPositiveButton(android.R.string.ok) { _, _ ->
                startIntent(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
            create()
            show()
        }
    }

    private fun askForDataWhitelisting() {
        AlertDialog.Builder(this).apply {
            setMessage(R.string.warn_background_data)
            setCancelable(false)
            setPositiveButton(R.string.allow_background_data) { _, _ -> intentIgnoreDataSaver() }
            setNegativeButton(R.string.cancel_allow_background_data) { dialog, _ -> logError(R.string.did_not_whitelist); dialog.cancel() }
            create()
            show()
        }
    }

    private fun askForBatteryWhitelisting() {
        AlertDialog.Builder(this).apply {
            setMessage(R.string.warn_battery_optimized)
            setCancelable(false)
            setPositiveButton(R.string.allow_no_optimize) { _, _ -> intentDoNotOptimizeBattery() }
            setNegativeButton(R.string.allow_optimize) { dialog, _ -> dialog.cancel() }
            create()
            show()
        }
    }

    private fun intentIgnoreDataSaver() {
        val intent = Intent(Settings.ACTION_IGNORE_BACKGROUND_DATA_RESTRICTIONS_SETTINGS)
        intent.data = Uri.parse("package:$packageName")
        startIntent(intent)
    }

    @SuppressLint("BatteryLife")
    private fun intentDoNotOptimizeBattery() {
        val intent = Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
        intent.data = Uri.parse("package:$packageName")
        startIntent(intent)
    }

    private fun startIntent(intent: Intent) {
        // we could check if it can be started with resolveActivity(). But that always returns != null
        // if there is any such activity even if we cannot start it (belongs to different package
        // and is not exported). This is too complicated - instead just ignore the exception.
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            logError { "$e" }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == locationRequested)
            Status.changedLocationPermission()
        // If request is cancelled, the result array is empty.
        if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED))
            tryStartForwarding()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.start_action)?.isVisible = !sender.isForwardingEnabled
        menu?.findItem(R.id.stop_action)?.isVisible = sender.isForwardingEnabled
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onBackPressed() {
        if (sender.isForwardingEnabled) {
            Toast.makeText(this, getString(R.string.please_stop_forwarding_first), Toast.LENGTH_SHORT).show()
        } else {
            super.onBackPressed()
        }
    }

    private fun notify(message: String) {
        // adapted from http://www.kotlincodes.com/kotlin/notifications-in-kodlin-oreo-8/

        NotificationCompat.Builder(this, ForegroundService.channelId).apply {
            setSmallIcon(R.drawable.smallic_gpsforwarder)
            setContentText(message)
            // setNumber(3) // this shows a number in the notification dots
        }.let {
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
                notify(ForegroundService.notificationId, it.build())
            }
        }
    }
}
