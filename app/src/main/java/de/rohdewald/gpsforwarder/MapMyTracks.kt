package de.rohdewald.gpsforwarder

import android.content.SharedPreferences
import android.location.Location
import android.os.Handler
import android.os.Looper
import android.util.Base64
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.w3c.dom.Document
import java.lang.Long.max
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory

private const val noMmtId = "0"

// even if the positions are all within prefMinDistance send last known position at least every 10 minutes
private const val pingInterval = 1L * 60L * 1000L

var altitudeAsCounter = false
var locationCount = 0.0
var requestInstanceCount = 0L


internal abstract class SendCommand(val location: Location? = null) {
    abstract val mmtRequestName: String
    abstract val expect: String
    var mmtId: String = noMmtId
    val allLocations: MutableList<Location> = mutableListOf()
    var queueTime = 0L
    private lateinit var response: String
    var parsedResponse: Document? = null
    var isResponseGood = false
    var request: StringRequest? = null

    init {
        if (location != null)
            allLocations.add(location)
    }

    abstract fun postDict(): HashMap<String, String>
    protected fun formatLocation(): String {
        // as expected by the MapMyTracks protocol
        return when (allLocations.size) {
            0 -> ""
            else -> allLocations.joinToString(separator = " ") { "${it.latitude} ${it.longitude} ${it.altitude} ${it.time / 1000}" }
        }
    }

    override fun toString(): String {
        var result = "Command($mmtRequestName ${allLocations.size} points"
        if (mmtId != noMmtId) result += " mmtId=$mmtId"
        return result + ")" + durationString()
    }

    private fun durationString() =
            if (queueTime > 0L)
                " in ${(System.currentTimeMillis() - queueTime).asTimeDeltaString()}"
            else ""

    fun addLocation(additional_location: Location) {
        allLocations.add(additional_location)
    }

    fun locationsToString(): String =
            if (altitudeAsCounter) {
                val altitudes = allLocations.map { it.altitude.toInt() }
                if (altitudes.size == 1)
                    appContext.resources.getString(R.string.forwarded_one_position, altitudes[0])
                else
                    appContext.resources.getString(R.string.forwarded_positions, allLocations.size, altitudes.min(), altitudes.max())
            } else {
                appContext.resources.getQuantityString(R.plurals.points_num, allLocations.size, allLocations.size)
            }

    abstract fun toLogStringCore(answer: String): String

    private fun toLogString(answer: String) = "${if (answer == expect) toLogStringCore(answer) else "$this got $answer"} ${durationString()}"

    open fun prepareForTransmit(currentMmtId: String) {
        mmtId = currentMmtId
    }

    open fun doAfterSuccessfulTransmit(mmt: MapMyTracks, gotResponse: String) {
        response = gotResponse
        mmt.lastSentTime = System.currentTimeMillis()
        parsedResponse = parseString(response)
        parsedResponse?.type()?.let { type ->
            if (type == expect) {
                isResponseGood = true
                logSend { toLogString(type) }
            } else {
                logError { appContext.getString(R.string.unexpected_answer, expect, type, parsedResponse?.reason()) }
                mmt.resetAll()
            }
        }
    }

    private fun parseString(networkResponse: String): Document? {
        val documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        return try {
            documentBuilder.parse(networkResponse.byteInputStream(Charsets.UTF_8))
        } catch (e: Exception) {
            null
        }
    }

}


internal class SendStart(location: Location?) : SendCommand(location) {
    override val mmtRequestName = "start_activity"
    override val expect = "activity_started"
    override fun postDict() = hashMapOf(
            "request" to mmtRequestName,
            "title" to "Life track from GPS Forwarder",
            "privacy" to "private",
            "activity" to "walking",
            "source" to "GPS Forwarder",
            "version" to "${BuildConfig.VERSION_NAME} ${BuildConfig.VERSION_CODE}",
            "points" to formatLocation())

    override fun toLogStringCore(answer: String): String = appContext.getString(R.string.forwarding_started, mmtId, locationsToString())

    override fun doAfterSuccessfulTransmit(mmt: MapMyTracks, gotResponse: String) {
        super.doAfterSuccessfulTransmit(mmt, gotResponse)
        if (isResponseGood)
            mmt.gotMmtId(parsedResponse?.activityId())
    }
}

internal open class SendUpdate(location: Location?) : SendCommand(location) {
    override val mmtRequestName = "update_activity"
    override val expect = "activity_updated"
    override fun postDict() = hashMapOf(
            "request" to mmtRequestName,
            "activity_id" to mmtId,
            "points" to formatLocation())

    override fun toLogStringCore(answer: String): String = appContext.getString(R.string.forwarded, mmtId, locationsToString())
}

internal class SendPing(location: Location?) : SendUpdate(location) {
    override fun toLogStringCore(answer: String): String = appContext.getString(R.string.ping_sent, mmtId, pingInterval.asTimeDeltaString())
}

internal class SendStop : SendCommand() {
    override val mmtRequestName = "stop_activity"
    override val expect = "activity_stopped"
    override fun postDict() = hashMapOf(
            "request" to mmtRequestName,
            "activity_id" to mmtId)

    override fun toLogStringCore(answer: String): String = appContext.getString(R.string.forwarding_stopped, mmtId)

    override fun doAfterSuccessfulTransmit(mmt: MapMyTracks, gotResponse: String) {
        super.doAfterSuccessfulTransmit(mmt, gotResponse)
        if (isResponseGood) {
            mmt.forwardingHasFinished()
        }
    }
}

class Scheduler(val action: () -> Unit) {

    private var currentInterval: Long = 0L  // the current interval in ms
    private var handler: Handler? = null

    var nextInterval = 10000L    // the next interval to be used as default in ms, changes if prefs change

    fun isRunning() = (handler != null)

    // if the interval changes,
    // create a new handler and use that one from now on. After the
    // latest event from the old handler happens, it will stay silent and be
    // garbage collected.

    fun run() {
        // schedule the next timer
        assert(nextInterval >0)
        handler = null
        logStartStop { appContext.getString(R.string.scheduler_start, nextInterval.asTimeDeltaString()) }
        currentInterval = nextInterval
        loop()
    }

    fun idle() {
        if (handler != null) {
            logMessage(LogType.Scheduler) { appContext.getString(R.string.scheduler_idle) }
            handler = null
        }
    }

    private fun loop() {
        handler = Handler().apply {
            val thisHandler = this
            val runnable = object : Runnable {
                override fun run() {
                    if (thisHandler == handler)
                        // if timing intervals change, we just start a new handler.
                        // ignore the old handler when it still calls us.
                        // otherwise we'd have to keep a reference to the old and the new runnable
                        // and use handler.removeCallbacks(runnable)
                        doAndSleep(this, action, currentInterval)
                }
            }
            doAndSleep(runnable, action, currentInterval)
        }
    }
}

class MapMyTracks {

    // we do not really need this queue: We make sure it never holds more than 1 request.
    // because we combine outstanding positions into one request. So if a request fails,
    // rebuild it with all outstanding positions.
    private var queue = Volley.newRequestQueue(appContext.applicationContext)

    // I would like to ask Volley if there are outstanding requests. But RequestQueue does
    // not seem to have such an attribute
    private var isSending: Boolean = false

    // this is our real queue.
    private var commands: MutableList<SendCommand> = mutableListOf()

    //isForwardingEnabled should always correspond to the button states in mainActivity
    var isForwardingEnabled = false

    // isForwarding is true after send() and until command SendStop() is confirmed by server.
    // This survives process restart.
    private var isForwarding = false

    var isResuming = false

    private var lastQueuedLocation: Location? = null
    internal var lastSentTime = 0L
    private var lastSeenLocation: Location? = null

    private lateinit var prefUrl: String
    private lateinit var prefUsername: String
    private lateinit var prefPassword: String
    private var prefMinDistance = 0
    private var prefUpdateIntervalSeconds = 2L
    private var prefUpdateIntervalMeters = 1000L
    private val maxPointsPerTransfer = 200  // more produces too many timeouts

    // currentMmtId should be set after SendStart() returns its value from the server.
    // It should be unset only after the server confirms SendStop() because this process
    // might die before the server answer has been processed. currentMmtId is persistent
    // in preferences. So if we unset it too soon, the server might never stop the activity.
    var currentMmtId: String = noMmtId
    private val scheduler = Scheduler(::transmit)

    fun initialize() {
        resetAll()
        preferenceChanged(prefs)
        currentMmtId = prefs.getString("MmtId", noMmtId) ?: noMmtId
        logError { "MapMyTracks.initialize: using MmtId $currentMmtId from prefs"}
        isResuming = hasMmtId()
        if (isResuming) {
            logError("MMT init() found MmtId $currentMmtId, noMmtId=$noMmtId")
        }
    }

    internal fun resetAll() {
        sleep()
        commands.clear()
        isForwarding = false
        currentMain?.invalidateOptionsMenu()
        lastSeenLocation = null
        lastQueuedLocation = null
        lastSentTime = 0L
    }

    fun forwardingHasFinished() {
        // clear isForwarding before clearing mmtId because
        // changing mmtId will trigger changed preferences which
        // triggers scheduler.restart()
        isForwarding = false
        gotMmtId(noMmtId)
    }

    private fun hasMmtId() = currentMmtId != noMmtId

    private fun queueCommand(command: SendCommand) {
        commands.add(command)
        if (command.allLocations.size > 0)
            lastQueuedLocation = command.allLocations.last()
    }

    fun send(location: Location) {
        if (lastQueuedLocation == null)
            logStartStop(R.string.sending_first_location)
        if (!isForwarding) {
            isForwarding = true
            startWith(location)
        } else {
            forward(location)
            if (status.isNetUsable && !scheduler.isRunning())
                scheduler.run()
        }
    }

    fun wakeUp() {
        if (isForwarding) {
            logStartStop { appContext.getString(R.string.wakeup, unsentPositions()) }
            currentMain?.startLocationRequests()
            if (unsentPositions() > 0) {
                scheduler.run()
                networkUpAt = System.currentTimeMillis()
            } else
                sendLastSeenLocation()
        }
    }

    fun preferenceChanged(prefs: SharedPreferences?) {
        if (prefs != null) {
            prefUrl = prefs.getString("pref_key_url", null)
                    ?: "Please define the URL in server settings"
            prefUsername = prefs.getString("pref_key_username", null)
                    ?: "Please define the user name in server settings"
            prefPassword = prefs.getString("pref_key_password", null)
                    ?: "Please define the password in server settings"
            altitudeAsCounter = prefs.getBoolean("pref_key_elevation_counter", false)
            prefUpdateIntervalSeconds = (prefs.getString("pref_key_update_interval_seconds", null)
                    ?: "10").toLong()
            if (scheduler.nextInterval != prefUpdateIntervalSeconds * 1000L) {
                val prevMs = scheduler.nextInterval
                scheduler.nextInterval = prefUpdateIntervalSeconds * 1000L
                if (scheduler.isRunning() && isForwarding) {
                    // We may be called by StartCommand.doAfterSuccessfulTransmit -> gotMmtId -> prefs.putString("MmtId")
                    // and we must finish that before restarting the scheduler
                    logStartStop { appContext.getString(R.string.update_interval_changed_restart, prevMs.asTimeDeltaString(), scheduler.nextInterval.asTimeDeltaString()) }
                    Handler(Looper.getMainLooper()).post { scheduler.run() }
                }
            }
            prefUpdateIntervalMeters = (prefs.getString("pref_key_update_interval_meters", null)
                    ?: "1000").toLong()
            prefMinDistance = (prefs.getString("pref_key_min_distance", null)
                    ?: "20").toInt()
        }
    }

    private fun startWith(location: Location) {
        if (isResuming) {
            isResuming = false
            logGpsFix { "startWith resuming with $location" }
            queueCommand(SendUpdate(location))
        } else {
            logGpsFix { "startWith not resuming with $location" }
            queueCommand(SendStart(location))
            transmit()
        }
    }

    private fun timeSinceLastSendRequest(): Long {
        lastQueuedLocation?.apply {
            return System.currentTimeMillis() - max(time, networkUpAt)
        }
        // should never happen
        return pingInterval
    }

    private fun timeSinceLastSendSuccess(debug: Boolean = false): Long {
        var lastSeenTime = 0L
        lastSeenLocation?.apply {
            lastSeenTime = time
        }
        val result = System.currentTimeMillis() - maxOf(lastSentTime, networkUpAt, lastSeenTime)
        if (debug)
            logStartStop { "timeSinceLastSendSuccess: ${result.asTimeDeltaString()} = ${System.currentTimeMillis().asDateTimeString()} - maxOf(${lastSentTime.asDateTimeString()},${networkUpAt.asDateTimeString()},${lastSeenTime.asDateTimeString()})" }
        return result
    }

    private fun sendLastSeenLocation() {
        lastSeenLocation?.apply {
            logSend { "Sending Ping" }
            queueCommand(SendPing(lastSeenLocation))
            transmit()
        }
    }

    private fun forward(location: Location) {
        val distance = location.distanceTo(lastQueuedLocation)
        val maxInterval = pingInterval + scheduler.nextInterval
        if (distance >= prefMinDistance) {
            // ignore small movements
            queueCommand(SendUpdate(location))
            if (timeSinceLastSendRequest() > prefUpdateIntervalSeconds * 1000 || distance >= prefUpdateIntervalMeters) {
                // send if minimum time or meters are reached
                transmit()
            }
        }

        if (timeSinceLastSendRequest() > maxInterval && status.isNetUsable && !isSending) {
            // even where there is no movement, occasionally send a ping with the current position
            // this is a legal situation
            sendLastSeenLocation()
        } else if (timeSinceLastSendSuccess() > maxInterval && isForwarding) {
            // something might hang, this is not a legal situation
            logError("sending took ${timeSinceLastSendSuccess(debug=true).asTimeDeltaString()} maxInterval: ${maxInterval.asTimeDeltaString()} Scheduler.nextInterval: ${scheduler.nextInterval.asTimeDeltaString()} pingInterval: ${pingInterval.asTimeDeltaString()}, polling network status and restarting scheduler" )
            Status.poll()
            scheduler.run()   // maybe the scheduler hung up itself
        }
        lastSeenLocation = location
    }

    internal fun gotMmtId(newId: String?) = when (newId) {
        null -> {
            logError { "Server responded to start_activity but returned no activity_id" }
            resetAll()
        }
        noMmtId -> {
            logError("Server sent noMmtId, replacing currentMmtId $currentMmtId also in prefs")
            currentMmtId = noMmtId
            prefs.putString("MmtId", noMmtId)
            resetAll()
        }
        currentMmtId -> {
            logError { "start_activity: Server sent the same activity_id $newId again" }
            queueCommand(SendStop())
            transmit()
        }
        else -> {
            logError("Server sent $newId, replacing $currentMmtId also in prefs")
            currentMmtId = newId
            prefs.putString("MmtId", newId)
            commands.forEach { it.mmtId = newId }
            logStartStop { appContext.getString(R.string.get_new_server_id, newId) }
        }
    }

    fun start() {
        isForwardingEnabled = true
    }

    fun stop() {
        if (isForwardingEnabled) {
            isForwardingEnabled = false
            if (hasMmtId()) {
                queueCommand(SendStop())
                transmit()
            } else {
                // stop() called before start() got an answer from server
                resetAll()
            }
        }
    }

    private fun parseError(networkResponse: NetworkResponse): Document? {
        val documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        return try {
            documentBuilder.parse(networkResponse.data.inputStream())
        } catch (e: Exception) {
            null
        }
    }

    // amount of positions still to be forwarded. This should not be called while isSending.
    private fun unsentPositions() = commands.map { it.allLocations.size }.sum()

    private fun combine() {
        val newCommand = commands[0]
        if (newCommand is SendStop) return
        if (newCommand.allLocations.size >= maxPointsPerTransfer) return
        val added: MutableList<SendCommand> = mutableListOf()
        for (command in commands.subList(1, commands.size)) {
            if (command !is SendUpdate) continue
            if (command.location == null) continue
            if (newCommand.allLocations.size >= maxPointsPerTransfer) break
            newCommand.addLocation(command.location)
            added.add(command)
        }
        commands = commands.filter { it !in added }.toMutableList()
    }

    private fun transmit() {
        if (commands.size == 0) {
            return
        }
        if (isSending) {
            logSend { "transmit is already sending" }
            return
        }
        combine()
        assert(commands.size == 0) { "combine returns 0 commands" }
        if (!status.isNetUsable) {
            logSend { "transmit thinks net is unusable: ${status.network} ${status.netString}"}
            return
        }
        val command: SendCommand = commands[0]
        command.prepareForTransmit(currentMmtId)
        val request = object : StringRequest(Method.POST, "$prefUrl/api/",
                Response.Listener { response ->
                    isSending = false
                    command.doAfterSuccessfulTransmit(this, response)
                    dropCommand()
                    if (unsentPositions() > 1) {
                        // if more is queued, send it right away. But not single positions.
                        transmit()
                    }
                },
                Response.ErrorListener { response ->
                    isSending = false
                    when {
                        response is AuthFailureError -> {
                            logError { appContext.getString(R.string.auth_failed, prefUrl, prefUsername) }
                            resetAll()
                        }
                        response.networkResponse != null -> {
                            val document = parseError(response.networkResponse)
                            when {
                                document != null -> logError { appContext.getString(R.string.received_error, prefUrl, response, document.reason(), command) }
                                "Service Unavailable" in String(response.networkResponse.data) -> serverNotAvailable(response)
                                else -> logError { "unknown networkResponse: ${response.networkResponse.statusCode} ${String(response.networkResponse.data)}" }
                            }
                        }
                        "NoConnectionError" in response.toString() -> lostConnection(response)
                        else -> logError { "unknown response: $prefUrl: $response for $command" }
                    }
                }) {
            override fun getParams(): MutableMap<String, String> = command.postDict()
            override fun getHeaders(): Map<String, String> = hashMapOf(
                    "Authorization" to "Basic " + Base64.encodeToString("$prefUsername:$prefPassword".toByteArray(Charsets.UTF_8), Base64.DEFAULT))
        }
        request.retryPolicy = DefaultRetryPolicy(30000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        command.request = request
        queue.add(request)
        isSending = true
        command.queueTime = System.currentTimeMillis()
    }

    private fun lostConnection(error: VolleyError) {
        logStartStop { "lostConnection: error: $error network: ${status.network} ${status.netString} netUsable: ${status.isNetUsable}" }
        if (commands[0] is SendStart) {
            resetAll()
        }
    }

    private fun serverNotAvailable(error: VolleyError) {
        logError { appContext.getString(R.string.server_not_available, error.networkResponse.statusCode) }
        if (commands[0] is SendStart)
            resetAll()
    }

    fun sleep() {
        if (isSending) {
            logStartStop { if (commands.size > 0) appContext.getString(R.string.cancel_sending_request, commands[0]) else appContext.getString((R.string.cancel_sending)) }
        }
        scheduler.idle()
        commands.clear()
        isSending = false
        currentMain?.stopLocationRequests()
        queue.cancelAll { true }
    }

    private fun dropCommand() {
        commands = commands.drop(1).toMutableList()
    }
}

fun Document.firstOf(key: String): String? {
    val reasonItem = getElementsByTagName(key)
    return if (reasonItem.length != 0) reasonItem.item(0).textContent else null
}

fun Document.reason() = firstOf("reason") ?: "no reason given"

fun Document.type() = firstOf("type")

fun Document.activityId(): String {
    val result = firstOf("activity_id")
    require(result is String)
    return result
}
