package de.rohdewald.gpsforwarder


import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.*
import android.os.BatteryManager
import android.os.PowerManager
import android.provider.Settings

var status = Status()

var networkUpAt = 0L

val connectivityManager by lazy { appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
val powerManager by lazy { appContext.getSystemService(Context.POWER_SERVICE) as PowerManager }
val batteryManager by lazy { appContext.getSystemService(Context.BATTERY_SERVICE) as BatteryManager }
val locationManager: LocationManager by lazy { appContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager }

var instanceCount = 0L

/* Airplane mode does not mean there is no network
  see https://www.xda-developers.com/customize-radios-airplane-mode-android/
  so we just watch the status but it has no consequences
 */

data class Status(
        val instanceId: Long = instanceCount.also { instanceCount += 1 },
        val isBatteryOptimized: Boolean = false,
        val isSavingPower: Boolean = false,
        // isDeviceIdle is true if the device is in deep sleep. See
        // adb shell dumpsys battery unplug
        // adb shell dumpsys deviceidle step
        // We do not try to make best use of the maintenance window because Oruxmaps
        // does not deliver GPX fixes anyway in deep sleep.
        val isDeviceIdle: Boolean = false,
        val isInAirplaneMode: Boolean = false,
        val isForeground: Boolean = false,
        val network: Network? = null,
        val networkCapabilities: NetworkCapabilities? = null,
        val restrictBackgroundStatus: Int = -999,
        val isLocationEnabled: Boolean = false,
        val isPowerConnected: Boolean = false,
        val isCharging: Boolean = false) {

    lateinit var prevStatus: Status

    val isNetUsable by lazy {
        // THIS MUST ONLY USE LOCAL DATA, this is an immutable snapshot
        when {
            restrictBackgroundStatus == -999 -> false
            network == null -> false
            networkCapabilities == null -> false
            !isValidated -> false
            isDeviceIdle -> false
            !isNetworkMetered -> true
            currentMain != null -> true
            !isDataSaving -> true
            else -> false
        }
    }

    private val reasonForUnusableNet by lazy {
        "[${prevStatus.instanceId}] -> [${instanceId}] ${when {
            // THIS MUST ONLY USE LOCAL DATA, this is an immutable snapshot
            restrictBackgroundStatus == -999 -> appContext.getString(R.string.status_not_yet_known)
            network == null -> appContext.getString(R.string.no_network)
            networkCapabilities == null -> appContext.getString(R.string.network_capabilities_unknown)
            !isValidated -> appContext.getString((R.string.net_not_validated))
            isDeviceIdle -> appContext.getString(R.string.device_is_idle)
            else -> appContext.getString(R.string.reason_unknown)
        } } "
    }

    private val isValidated by lazy {
        networkCapabilities?.hasCapability(NET_CAPABILITY_VALIDATED) ?: false
    }

    private val isNetworkMetered by lazy {
        !(networkCapabilities?.hasCapability(NET_CAPABILITY_NOT_METERED) ?: false)
    }

    val hasLocationPermission by lazy { getHasLocationPermission() }

    val netBecameUsable by lazy { !prevStatus.isNetUsable && isNetUsable }

    val netBecameUnusable by lazy { prevStatus.isNetUsable && !isNetUsable }

    private val transportName by lazy {
        if (networkCapabilities != null) {
            when {
                networkCapabilities.hasTransport(TRANSPORT_CELLULAR) -> appContext.getString(R.string.cellular)
                networkCapabilities.hasTransport(TRANSPORT_WIFI) -> appContext.getString(R.string.wifi)
                else -> appContext.getString(R.string.other_transport)
            }
        } else
            "No network"
    }

    val netString by lazy {
        val meteredString = if (isNetworkMetered) appContext.getString(R.string.network_metered) else appContext.getString(R.string.network_unmetered)
        if (!hasNet()) appContext.getString(R.string.no_internet) else "$transportName $meteredString"
    }

    val stateString by lazy {
        when {
            restrictBackgroundStatus == -999 -> appContext.getString(R.string.status_not_yet_known)
            network == null -> appContext.getString(R.string.no_network)
            networkCapabilities == null -> appContext.getString(R.string.network_capabilities_unknown)
            !networkCapabilities.hasCapability(NET_CAPABILITY_INTERNET) -> appContext.getString(R.string.no_internet)
            !isValidated -> appContext.getString(R.string.net_not_validated)
            !isForeground && isDataSaving && isNetworkMetered -> appContext.getString(R.string.network_disabled_in_background_metered)
            else -> appContext.getString(R.string.network_fully_usable)
        }
    }

    val stateStringWithId by lazy {
        "[${prevStatus.instanceId}] -> [$instanceId] $stateString"
    }

    private fun hasNet() = (networkCapabilities?.hasCapability((NET_CAPABILITY_INTERNET)) ?: false
    and isValidated)

    val isDataSaving by lazy { restrictBackgroundStatus == ConnectivityManager.RESTRICT_BACKGROUND_STATUS_ENABLED }

    private val isBatteryOptimizedString by lazy { appContext.getString(if (isBatteryOptimized) R.string.app_battery_optimizer_on else R.string.app_battery_optimizer_off) }

    private val isSavingPowerString by lazy { appContext.getString(if (isSavingPower) R.string.battery_saver_on else R.string.battery_saver_off) }

    private val isInAirplaneModeString by lazy { appContext.getString(if (isInAirplaneMode) R.string.airplane_mode_is_on else R.string.airplane_mode_is_off) }

    private val isDataSavingString by lazy { appContext.getString(if (isDataSaving) R.string.data_saver_is_on else R.string.data_saver_is_off) }

    private val isDeviceIdleString by lazy { appContext.getString(if (isDeviceIdle) R.string.device_is_idle else R.string.device_is_not_idle) }

    private val isLocationEnabledString by lazy { appContext.getString(if (isLocationEnabled) R.string.location_is_enabled else R.string.location_is_disabled) }

    private val isPowerConnectedString by lazy { appContext.getString(if (isPowerConnected) R.string.is_power_connected else R.string.is_power_disconnected) }

    private val isChargingString by lazy { appContext.getString(if (isCharging) R.string.is_charging else R.string.is_discharging) }

    companion object {

        private lateinit var networkCallback: ConnectivityManager.NetworkCallback
        private lateinit var defaultNetworkCallback: ConnectivityManager.NetworkCallback

        fun register() {
            val firstStatus = Status()  // first create firstStatus, giving it instanceId 1
            status = from()
            if (status.isNetUsable)
                networkUpAt = System.currentTimeMillis()
            status.prevStatus = firstStatus
            status.logStatusDiffs()
            val filter = IntentFilter(Intent.ACTION_POWER_CONNECTED)
            filter.addAction(Intent.ACTION_POWER_DISCONNECTED)
            filter.addAction(BatteryManager.ACTION_CHARGING)
            filter.addAction(BatteryManager.ACTION_DISCHARGING)
            filter.addAction(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED)
            filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED)
            filter.addAction(LocationManager.MODE_CHANGED_ACTION)   // LocationManager
            filter.addAction(ConnectivityManager.ACTION_RESTRICT_BACKGROUND_CHANGED)
            filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)

            appContext.registerReceiver(intentReceiver, filter)

            defaultNetworkCallback = object : ConnectivityManager.NetworkCallback() {

                override fun onCapabilitiesChanged(network: Network, networkCapabilities: NetworkCapabilities) {
                    super.onCapabilitiesChanged(network, networkCapabilities)
                    networkCapabilities.apply {
                        // not interested in a new connection while it is not yet validated
                        if (hasCapability(NET_CAPABILITY_INTERNET)) {
                            if (!hasCapability(NET_CAPABILITY_VALIDATED)) return
                        }
                    }
                    useNewStatus(from(network, networkCapabilities), "onCapabilitiesChanged")
                }
            }
            connectivityManager.registerDefaultNetworkCallback(defaultNetworkCallback)
        }

        fun unregister() {
            try {
                appContext.unregisterReceiver(intentReceiver)
            } catch (e: IllegalArgumentException) {
            }
            try {
                connectivityManager.unregisterNetworkCallback(networkCallback)
            } catch (e: UninitializedPropertyAccessException) {
            }
        }

        private val intentReceiver = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent) {
                gotEvent(intent)
            }
        }

        private fun getIsInAirplaneMode() =
                Settings.Global.getInt(appContext.contentResolver,
                        Settings.Global.AIRPLANE_MODE_ON, 0) != 0

        private fun getPowerConnected(): Boolean {
            val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { intentFilter ->
                appContext.registerReceiver(null, intentFilter)
            }
            val chargePlug: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) ?: -1
            return chargePlug == BatteryManager.BATTERY_PLUGGED_USB || chargePlug == BatteryManager.BATTERY_PLUGGED_AC || chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS
        }

        private fun from(newNetwork: Network, newCapabilities: NetworkCapabilities) = status.clone().copy(
                network = newNetwork,
                networkCapabilities = newCapabilities
        ).also { try {
            if (it.netString != it.prevStatus.netString) logStartStop { "[${it.instanceId}] found network ${it.network} ${it.netString}" }
        } catch (e: Exception) {
        }
        }

        // from() is only called once when the app starts
        private fun from() = Status(
                isBatteryOptimized = !powerManager.isIgnoringBatteryOptimizations(appContext.packageName),
                isSavingPower = powerManager.isPowerSaveMode,
                isDeviceIdle = powerManager.isDeviceIdleMode,
                isInAirplaneMode = getIsInAirplaneMode(),
                isForeground = currentMain != null,
                network = connectivityManager.activeNetwork,
                networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork),
                restrictBackgroundStatus = getRestrictBackgroundStatus(),
                isLocationEnabled = getIsLocationEnabled(),
                isPowerConnected = getPowerConnected(),
                isCharging = batteryManager.isCharging
        ).also { logStartStop { "from()"} }

        private fun from(intent: Intent) = when (intent.action) {
            Intent.ACTION_POWER_CONNECTED -> status.clone().copy(isPowerConnected = true)
            Intent.ACTION_POWER_DISCONNECTED -> status.clone().copy(isPowerConnected = false)
            Intent.ACTION_AIRPLANE_MODE_CHANGED -> status.clone().copy(isInAirplaneMode = getIsInAirplaneMode()).also { logStartStop { status.isInAirplaneModeString } }
            BatteryManager.ACTION_CHARGING -> status.clone().copy(isCharging = true)
            BatteryManager.ACTION_DISCHARGING -> status.clone().copy(isCharging = false)
            PowerManager.ACTION_POWER_SAVE_MODE_CHANGED -> status.clone().copy(isSavingPower = powerManager.isPowerSaveMode)
            PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED -> status.clone().copy(isDeviceIdle = powerManager.isDeviceIdleMode)
            LocationManager.MODE_CHANGED_ACTION -> status.clone().copy(isLocationEnabled = getIsLocationEnabled())
            ConnectivityManager.ACTION_RESTRICT_BACKGROUND_CHANGED -> status.clone().copy(
                    // can I have a separate intent for isIgnoringBatteryOptimization please?
                    restrictBackgroundStatus = getRestrictBackgroundStatus(),
                    isBatteryOptimized = !powerManager.isIgnoringBatteryOptimizations(appContext.packageName)
            )
            else -> {
                logError { "Unhandled: $intent" }
                status
            }
        }.also { logStartStop { "[${it.instanceId}] from intent $intent"}}

        internal fun getHasLocationPermission() =
                appContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        &&     appContext.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED

        private fun getIsLocationEnabled() =
                if (!getHasLocationPermission()) {
                    false
                } else {
                    locationManager.isLocationEnabled
                }

        private fun getRestrictBackgroundStatus() = connectivityManager.restrictBackgroundStatus

        /* Optimally, no polling should be needed, we should get intents for all relevant changes.
           That is not so:
           - I still struggle to understand the new paradigm. OnLosing/OnLost is not always called,
             like when the user disables wifi and mobile
           - isIgnoringBatteryOptimization is important for us but changes do not trigger intents
           We poll only when there was no successful connection for 5 minutes
           I am aware that using both intents (asynchronous) and polling (synchronous) may lead to
           races, see https://developer.android.com/reference/android/net/NetworkInfo.html
           But for now let's see if that ever happens
         */
        fun poll() = useNewStatus(from(), "poll").apply {
            // consistency check. Those attribute changes should already have been seen by non-polling means
            if (netString != prevStatus.netString) {
                logError("Polling found inconsistency in netString: [${prevStatus.instanceId}] ${prevStatus.netString} -> [$instanceId] $netString")
                logStatusDiffs(whereFrom = "new [${instanceId}]: ", full = true)
            } else if (stateString != prevStatus.stateString)
                logError("Polling found inconsistency in network state: ${prevStatus.stateStringWithId} -> $stateStringWithId")
            if (isBatteryOptimized == prevStatus.isBatteryOptimized) {
                // no check if one of the attributes needing polling changed
                if (isDeviceIdle != prevStatus.isDeviceIdle)
                    logError("Polling found inconsistency: ${prevStatus.isDeviceIdleString} -> $isDeviceIdleString")
                else if (isNetUsable != prevStatus.isNetUsable) {
                    logError("Polling found inconsistency for isNetUsable: ${prevStatus.isNetUsable} -> $isNetUsable")
                    if (!isNetUsable)
                        logNotUsable("poll")
                }
            }
        }

        fun gotEvent(intent: Intent) = useNewStatus(from(intent), "intent")

        fun changedLocationPermission() = useNewStatus(status.clone().copy(isLocationEnabled = getIsLocationEnabled()), whereFrom = "changedLocationPermission")

        fun useNewStatus(newStatus: Status, whereFrom: String = "") = newStatus.apply {
            prevStatus = status
            status = this
            logStatusDiffs(whereFrom)
            if (netBecameUsable) {
                sender.wakeUp()
            }
            if (netBecameUnusable)
                sender.sleep()
        }
    }

    private fun logNotUsable(whereFrom: String = "") = logError { whereFrom + " " + appContext.getString(R.string.no_forwarding, reasonForUnusableNet) }

    private fun idTransition() = if (prevStatus.instanceId == 1L) "[$instanceId]" else "[${prevStatus.instanceId}]->[${instanceId}]"

    private fun logStatusDiffs(whereFrom: String = "", against: Status = prevStatus, full: Boolean = false) {

        fun logDiff(message: String, errorPrefix: String = whereFrom, isError: () -> Boolean = { false }) {
            val logFunc: (() -> String) -> Unit = when (if (isError() || errorPrefix != "") LogType.Error else LogType.StartStop) {
                LogType.Error -> ::logError
                else -> ::logStartStop
            }
            if (errorPrefix == "")
                logFunc { message }
            else
                logFunc { "${idTransition()} $errorPrefix $message" }
        }

        val always = (full or (prevStatus.instanceId == 1L))
        if (always || against.isBatteryOptimized != isBatteryOptimized) logDiff(isBatteryOptimizedString, errorPrefix = "") { isBatteryOptimized }
        if (always || against.isSavingPower != isSavingPower) logDiff(isSavingPowerString) { isSavingPower }
        if (always || against.isDeviceIdle != isDeviceIdle) logDiff(isDeviceIdleString)
        if (always || against.isLocationEnabled != isLocationEnabled) logDiff(isLocationEnabledString) { !isLocationEnabled }
        if (always || against.isDataSaving != isDataSaving) logDiff(isDataSavingString) { isDataSaving }
        if (always || against.isCharging != isCharging) logDiff(isChargingString)
        if (always || against.isPowerConnected != isPowerConnected) logDiff(isPowerConnectedString)
        if (always) {
            if (hasNet())
                logDiff(stateStringWithId)
        } else when {
            !against.isNetUsable && isNetUsable ->
                logStartStop { appContext.getString(R.string.connected_to, stateStringWithId)  + " " + whereFrom}
            against.isNetUsable && !isNetUsable && !isDeviceIdle -> {
                // deviceIdle already logged with StartStop and it is not an error
                logNotUsable(whereFrom)
            }

            against.isNetUsable && isNetUsable && against.netString != netString ->
                logDiff(appContext.getString(R.string.connection_changed_to, against.netString, against.stateStringWithId, netString, stateStringWithId) + " " + whereFrom)
        }
    }

    fun clone(): Status {
        // I believe I cannot simply use copy() because that would not un-initialize lazy things
        return Status(
                isBatteryOptimized = isBatteryOptimized,
                isSavingPower = isSavingPower,
                isDeviceIdle = isDeviceIdle,
                isForeground = isForeground,
                network = network,
                networkCapabilities = networkCapabilities,
                restrictBackgroundStatus = restrictBackgroundStatus,
                isLocationEnabled = isLocationEnabled,
                isPowerConnected = isPowerConnected,
                isInAirplaneMode = isInAirplaneMode,
                isCharging = isCharging)
    }
}
