package de.rohdewald.gpsforwarder

import android.content.SharedPreferences
import android.location.Location
import android.os.Handler
import kotlin.jvm.internal.CallableReference
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

internal val timeFormat = SimpleDateFormat("HH:mm:ss.SSS", Locale.US)

fun Date.toLogString(): String? = timeFormat.format(this)

fun Location.toLogString(): String {
    val altitudeCounter = if (altitudeAsCounter) " ${altitude.toInt()}" else ""
    return "${"%.6f".format(latitude)} ${"%.6f".format(longitude)}$altitudeCounter"
}

fun Location.logTimes() {
    val bootTime = System.currentTimeMillis() - android.os.SystemClock.elapsedRealtime()
    val sysTime = System.currentTimeMillis()
    val gpsEtToTime = bootTime + elapsedRealtimeNanos / 1000000L
    val alt = if (altitudeAsCounter) " ${altitude.toInt()}" else ""
    logError { "$alt ${bootTime.asDateTimeString()} Boot time" }
    logError { "$alt ${sysTime.asDateTimeString()} System time" }
    logError { "$alt ${gpsEtToTime.asDateTimeString()} Boot time + location.elapsed" }
    logError("$alt ${time.asDateTimeString()} Location time")
}

fun SharedPreferences.putString(key: String, value: String) =
        edit()?.apply {
            putString(key, value)
            apply()
        }

fun Long.asDateTimeString() = SimpleDateFormat("YYYY dd.MM HH:mm:ss.SSS", Locale.US).format(this)
        ?: ""

fun Long.asTimeDeltaString() = when {
    this >= 60000L && (this % 60000L == 0L || this > 600000L) -> "${this / 60000L}min"
    this % 1000L == 0L || this > 10000L -> "${this / 1000L}s"
    this > 1000L -> "${"%.3f".format(this.toFloat() / 1000.0f)}s"
    else -> "${this}ms"
}

fun RecyclerView.scrollToEnd() {
    val ad = adapter
    if (ad != null)
        scrollToPosition(ad.itemCount - 1)
}

fun Handler.doAndSleep(runnable: Runnable, action: () -> Unit, sleepTime: Long) {
    logMessage(LogType.Scheduler) { appContext.getString(R.string.scheduler_calls, (action as CallableReference).name, sleepTime.asTimeDeltaString()) }
    try {
        action()
    } finally {
        postDelayed(runnable, sleepTime)
    }
}