package de.rohdewald.gpsforwarder

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.about_gpsforwarder.*

class AboutActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.about_gpsforwarder)
        app_version.text = getString(R.string.about_version, BuildConfig.GIT_TAG)
    }

    fun onClickWebsite(@Suppress("UNUSED_PARAMETER") view: View) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://rohdewald.de/gpsforwarder"))
        if (intent.resolveActivity(packageManager) != null)
            startActivity(intent)
    }

}
