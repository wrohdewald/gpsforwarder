package de.rohdewald.gpsforwarder

import android.content.Context
import android.graphics.Color
import android.graphics.Color.rgb
import android.os.Handler
import android.os.Looper.getMainLooper
import android.view.View
import android.view.ViewGroup
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.log_row.view.*
import java.io.File
import java.io.IOException
import java.util.*
import java.util.logging.*
import java.util.logging.Formatter

fun MainActivity.loggerPreferenceChanged() {
    logThis = readLogThis()
    FileLogger.initialize(appContext)
    val newCellTextSize = (prefs.getString("pref_key_fontsize", null) ?: "12").toFloat()
    val arrayId = when (newCellTextSize.toInt()) {
        8 -> R.array.logSpans8
        10 -> R.array.logSpans10
        14 -> R.array.logSpans14
        16 -> R.array.logSpans16
        else -> R.array.logSpans12
    }
    val newSpans = resources.getIntArray(arrayId).toMutableList()
    newSpans.add(100 - newSpans.sum())

    if (newCellTextSize != cellTextSize || newSpans != logSpans) {
        cellTextSize = newCellTextSize
        logSpans = newSpans
        logColumns = logSpans.size
        invalidateView(logView)
    }
}

class PlainTextFormatter : Formatter() {

    companion object {
        val DEFAULT = PlainTextFormatter()
    }

    override fun format(r: LogRecord): String {
        val builder = StringBuilder()
        builder.append("${r.millis.asDateTimeString()} TID=${r.threadID}  ${r.message}")

        r.thrown?.let {
            builder.append("\nEXCEPTION ${org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(it)}")
        }

        r.parameters?.let {
            for ((idx, param) in it.withIndex())
                builder.append("\n\tPARAMETER #").append(idx).append(" = ").append(param)
        }
        builder.append("\n")

        return builder.toString()
    }
}

object FileLogger {

    val log: Logger = Logger.getLogger("gpsforwarder")
    private var logFileName: String? = null


    fun initialize(context: Context) {

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val rootLogger = Logger.getLogger("")
        val logToFile = preferences.getBoolean("pref_key_external_logging", false)
        if (logFileName != null && !logToFile) {
            logStartStop(R.string.logging_to_external_storage_stopped)
            // Handler because logStartStop also uses a Handler
            Handler(getMainLooper()).post { removeHandlers(rootLogger) }
            logFileName = null
        } else if (logFileName == null && logToFile) {
            removeHandlers(rootLogger)
            val dir = context.getExternalFilesDir(null)
            if (dir != null)
                try {
                    logFileName = File(dir, "gpsforwarder-${android.os.Process.myPid()}-${System.currentTimeMillis().asDateTimeString()}.txt").toString()

                    val fileHandler = FileHandler(logFileName)
                    fileHandler.formatter = PlainTextFormatter.DEFAULT
                    rootLogger.addHandler(fileHandler)
                    logStartStop { context.getString(R.string.logging_to_external_storage, logFileName) }
                } catch (e: IOException) {
                    logError { context.getString(R.string.logging_could_not_create_file, e.localizedMessage) }
                }
        }
    }

    private fun removeHandlers(logger: Logger) {
        logger.apply {
            useParentHandlers = false
            handlers.forEach { removeHandler(it) }
            level = Level.ALL
        }
    }
}

class ScrollListener : RecyclerView.OnScrollListener() {
    private var lastY = 0
    var isAtEnd = true
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        lastY = dy
        if (dy < 0)
            isAtEnd = false
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        // TODO go to the middle of the list, scroll down: if a new entry is written, jumps to end
        // this does not happen when scrolling up
        if (newState == SCROLL_STATE_SETTLING && lastY > 0)
            isAtEnd = true

    }
}

fun MainActivity.setupLogger(isAlive: Boolean) {
    class MySpanSizeLookup : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int) = logSpans[position % logColumns]

        // we can optimize this because we know all rows have the same number of items
        override fun getSpanIndex(position: Int, spanCount: Int) =
                logSpans.subList(0, (position % logColumns)).sum()

    }

    val gridLayoutManager = GridLayoutManager(this, logSpans.sum())
    logView.layoutManager = gridLayoutManager
    gridLayoutManager.spanSizeLookup = MySpanSizeLookup()
    logAdapter = LogRecyclerAdapter(logItems)
    logView.adapter = logAdapter
    logView.addOnScrollListener(scroller)

    if (!isAlive)
        FileLogger.initialize(applicationContext)
}

fun logStartStop(msgCreator: () -> String) = logMessage(LogType.StartStop, msgCreator)
fun logGpsFix(msgCreator: () -> String) = logMessage(LogType.GpsFix, msgCreator)
fun logSend(msgCreator: () -> String) = logMessage(LogType.Send, msgCreator)
fun logError(msgCreator: () -> String) = logMessage(LogType.Error, msgCreator)

fun logStartStop(id: Int) = logMessage(LogType.StartStop, id)
fun logError(id: Int) = logMessage(LogType.Error, id)
fun logError(msg: String) = logMessage(LogType.Error) { msg }


enum class LogType {
    StartStop,
    GpsFix,
    Send,
    Error,
    Scheduler;

    companion object {
        private fun from(findValue: String): LogType {
            return try {
                valueOf(findValue)
            } catch (e: Exception) {
                try {
                    val typeArray = values()
                    typeArray[findValue.toInt()]
                } catch (e: Exception) {
                    Error
                }
            }
        }

        fun from(values: Set<String>): Set<LogType> {
            return values.map { from(it) }.toSet()
        }
    }
}


// The rest is internal

// this must correspond to what bindRow() does
internal var logSpans: List<Int> = listOf()
internal var logColumns = 2

internal val logItems = mutableListOf<LogItem>()

internal lateinit var logAdapter: LogRecyclerAdapter

internal fun invalidateView(view: RecyclerView?) =
        view?.apply {
            recycledViewPool.clear()
            invalidate()
            this.adapter?.notifyDataSetChanged()
        }

fun logMessage(type: LogType, msgCreator: () -> String) {
    if (type in logThis) Handler(getMainLooper()).post {
        val msg = msgCreator()
        val logType = if ("ERROR" in msg) LogType.Error else type
        logItems.add(LogItem(logType, msg))
        currentMain?.logView?.apply {
            adapter?.notifyItemRangeInserted(logItems.size * logColumns - logColumns, logColumns)
            currentMain?.apply {
                if (scroller.isAtEnd)
                    scrollToEnd()
            }
        }
        val logLevel = when (logType) {
            LogType.Error -> Level.SEVERE
            else -> Level.INFO
        }
        FileLogger.log.log(logLevel, msg)
    }
}

fun logMessage(type: LogType, id: Int) {
    logMessage(type) { appContext.getString(id) }
}

internal fun readLogThis(): Set<LogType> {
    val foundSettings = prefs.getStringSet("pref_key_log", null) ?: setOf()
    return LogType.from(foundSettings)
}

internal open class LogItem(val type: LogType, val msg: String) {
    val time = Date()
    //  val tid = android.os.Process.myTid()
}

internal var logThis = setOf<LogType>()
internal var cellTextSize = 0f

internal class LogRecyclerAdapter(private val logLines: List<LogItem>) : RecyclerView.Adapter<LogRecyclerAdapter.LogItemHolder>() {

    class LogItemHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view: View = v

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            // because it is abstract
        }

        fun bindRow(item: LogItem, position: Int) {
            val color = when (item.type) {
                LogType.Error -> Color.RED
                LogType.GpsFix -> Color.BLUE
                LogType.StartStop -> Color.MAGENTA
                LogType.Send -> rgb(0, 87, 74) // 0x00574a  - GREEN is not readable on white background
                else -> Color.BLACK
            }
            view.itemColumn.setTextColor(color)
            view.itemColumn.textSize = cellTextSize
            val column = position % logColumns
            view.itemColumn.text = when (column) {
                0 -> item.time.toLogString()
                1 -> item.msg
                else -> "Column $column"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogItemHolder {
        val inflatedView = parent.inflate(R.layout.log_row, false)
        return LogItemHolder(inflatedView)
    }

    override fun getItemCount() = logLines.size * logColumns

    override fun onBindViewHolder(holder: LogItemHolder, position: Int) {
        val item = logLines[position / logColumns]
        holder.bindRow(item, position)
    }
}

